/*******************************************************************************
 * CCSG MAX17262 Driver
 *
 * Copyright (c) 2020 by Bob Iannucci
 *******************************************************************************/

#include "CCSG_MAX17262.h"
#include "CCSG_Debug.h"
#include <Arduino.h>
#include <Wire.h>

#define MAX17262_REPEAT_READS 3


CCSG_MAX17262::CCSG_MAX17262(MAX17262_Lithium_Chemistry_t chem, uint16_t capmAh, float cv, float ev)
{
	lithiumChemistry = chem;
	softCapuAh = capmAh * 1000; // our own software capacity meter -- reinitialized at reset (better: keep it in flash and reset it explicitly)
	softCapuAhAtReset = softCapuAh;

	// Trying to print a debug message before ccsg_debug_init() causes a hang at boot time
	// if (capmAh > 6000)
	// {
	// 	CCSG_DEBUG_PRINTF("[max17262] Warning: specified capacity of %d mAh exceeds 6000 mAh capability of coulomb counter.\r\n", capmAh);
	// 	capmAh = 6000;
	// }
	capacitymAh = capmAh;
	capacitymAhFloat = (float)capmAh;
	chargeVolts = cv;
	emptyVolts = ev;
	recoveryVolts = 3.88;  // chip default
}


bool CCSG_MAX17262::init(TwoWire *i2c)
{
	i2cBus = i2c;
	uint32_t macroRetryCounter = 3;

	while(macroRetryCounter > 0)
	{	
		if (this->power_on_reset())
			return true;
		else
		{
			CCSG_DEBUG_PRINTLN("[max17262] Retrying power-on reset");
			macroRetryCounter--;
		}
	}
	CCSG_DEBUG_PRINTLN("[max17262] ERROR: Power-on-reset failed");
	return false;
}


// see https://pdfserv.maximintegrated.com/en/an/MAX1726x-Software-Implementation-user-guide.pdf
bool CCSG_MAX17262::power_on_reset()
{
	int statusPOR;
	int hibCfg;
	int status;
	uint16_t ve;  // really 9 bits on the chip
	uint8_t vr;   // really 7 bits on the chip
	uint16_t vevr;
	int repCap;
	int repSoC;
	int tte;

	int devNameReg = this->readRegisterWithRetry(MAX17262_DevName_Register, MAX17262_REPEAT_READS, 10);
	if (devNameReg == -1) return false;
	CCSG_DEBUG_PRINTF("[max17262] Device Name register: 0x%04X (should be 0x4039)\r\n", devNameReg);

	status = this->readRegisterWithRetry(MAX17262_Status_Register, MAX17262_REPEAT_READS, 10);
	if (status == -1) return false;
	CCSG_DEBUG_PRINTF("[max17262] Status register: 0x%04X\r\n", status);
	if ((status & 0x02) == 0) // test POR bit
	{
		CCSG_DEBUG_PRINTLN("[max17262] *** IC is NOT in power-on-reset (POR) state -- no reset needed");
		return true;
	}
	else
	{
		CCSG_DEBUG_PRINTLN("[max17262] *** IC is in power-on-reset (POR) state");
		int vEmptyReg = this->readRegisterWithRetry(MAX17262_VEmpty_Register, MAX17262_REPEAT_READS, 10);
		if (vEmptyReg == -1) return false;
		CCSG_DEBUG_PRINTF("[max17262] VEmpty register: 0x%04X (should be 0xA561)\r\n", vEmptyReg);
		this->writeRegister(MAX17262_Status_Register, status & 0xFFFD);  // turn off POR bit
	}

	CCSG_DEBUG_PRINTLN("[max17262] Performing coulomb counter power-on-reset");
	// Allow the IC to complete its power-up sequence
	int retryCounter = 10;
	status = this->readRegisterWithRetry(MAX17262_FStat_Register, MAX17262_REPEAT_READS, 10);
	if (status == -1)  return false;
	while (status & 0x01) 
	{
		if (retryCounter == 0)
		{
			CCSG_DEBUG_PRINTLN("[max17262] IC did not power up properly within the timeout");
			return false;
		}
		retryCounter--;
		delay(10); // msec
		status = this->readRegisterWithRetry(MAX17262_FStat_Register, MAX17262_REPEAT_READS, 10);
		if (status == -1)  return false;
	}
	CCSG_DEBUG_PRINTLN("[max17262] IC power-up sequence is complete");
	hibCfg = this->readRegisterWithRetry(MAX17262_HibCfg_Register, MAX17262_REPEAT_READS, 10);
	if (hibCfg == -1)  return false;
	// CCSG_DEBUG_PRINTLN("[max17262] A");
	this->writeRegister(MAX17262_Command_Register, 0x90);		// Exit hibernate mode step 1
	// CCSG_DEBUG_PRINTLN("[max17262] B");
	this->writeRegister(MAX17262_HibCfg_Register, 0x0);		// Exit hibernate mode step 2
	// CCSG_DEBUG_PRINTLN("[max17262] C");
	this->writeRegister(MAX17262_Command_Register, 0x0);				// Exit hibernate mode step 3
	// CCSG_DEBUG_PRINTLN("[max17262] D");

	// Option 2.1 in the datasheet: EZ Config
	this->writeRegister(MAX17262_DesignCap_Register, capacitymAh); // Write DesignCap
	// CCSG_DEBUG_PRINTLN("[max17262] E");
	this->writeRegister(MAX17262_IChgTerm_Register, 0x0640);       // default value -- 500 mA
	// CCSG_DEBUG_PRINTLN("[max17262] F");




	// ve is represented in 10 mV increments
	ve = (uint16_t)(emptyVolts * 100);
	// vr is represented in 40 mV increments
	vr = (uint8_t)(recoveryVolts * 25);
	vevr = (ve << 7) | (vr & 0x7F);
	this->writeRegister (MAX17262_VEmpty_Register, vevr); // Write VEmpty
	// CCSG_DEBUG_PRINTLN("[max17262] G");

	if (chargeVolts > 4.275)
		this->writeRegister (MAX17262_ModelCfg_Register, 0x8400); // Write ModelCFG
	else
		this->writeRegister (MAX17262_ModelCfg_Register, 0x8000); // Write ModelCFG
	// CCSG_DEBUG_PRINTLN("[max17262] H");

	delay(400);  // without a delay, many NAKs

	// do not continue until ModelCFG.Refresh == 0
	retryCounter = 10;
	int modelCfgReg = this->readRegisterWithRetry(MAX17262_ModelCfg_Register, MAX17262_REPEAT_READS, 10);
	if (modelCfgReg == -1) return false;
	while (modelCfgReg & 0x8000)
	{
		// CCSG_DEBUG_PRINTF("[max17262] ModelCfg register: 0x%04X\r\n", modelCfgReg);
		watchdogReset();

		if (retryCounter == 0)
		{
			CCSG_DEBUG_PRINTLN("[max17262] ModelCFG.Refresh flag did not go to zero within the timeout");
			return false;
		}
		retryCounter--;
		delay(10);
		modelCfgReg = this->readRegisterWithRetry(MAX17262_ModelCfg_Register, MAX17262_REPEAT_READS, 10);
		if (modelCfgReg == -1) return false;
	}
	// CCSG_DEBUG_PRINTLN("[max17262] I");
	this->writeRegister (MAX17262_HibCfg_Register, hibCfg); // Restore Original HibCFG value
	// CCSG_DEBUG_PRINTLN("[max17262] J");

	// Step 3 from the datasheet
	uint16_t statusPre = this->readRegisterWithRetry(MAX17262_Status_Register, MAX17262_REPEAT_READS, 100);
	CCSG_DEBUG_PRINTF("[max17262] Status register: 0x%04X\r\n", statusPre);
	this->writeRegisterWithRetry (MAX17262_Status_Register, statusPre & 0xFFFD, MAX17262_REPEAT_READS, 10); // clear POR bit

	int statusPost = this->readRegisterWithRetry(MAX17262_Status_Register, MAX17262_REPEAT_READS, 10);
	if (statusPost == -1) return false;
	// **DEBUG** -- remove this loop
	retryCounter = 10;
	while(statusPost != (statusPre & 0xFFFD)) 
	{
		if (retryCounter == 0)
		{
			CCSG_DEBUG_PRINTLN("[max17262] POR flag did not go to zero within the timeout");
			return false;
		}
		retryCounter--;
		statusPre = this->readRegisterWithRetry(MAX17262_Status_Register, MAX17262_REPEAT_READS, 10);
		if (statusPre == -1) return false;
		CCSG_DEBUG_PRINTF("[max17262] Status register: 0x%04X (after resetting POR)\r\n", statusPre);
		this->writeRegister (MAX17262_Status_Register, statusPre & 0xFFFD); // clear POR bit again
		statusPost = readRegisterWithRetry(MAX17262_Status_Register, MAX17262_REPEAT_READS, 10);
		if (statusPost == -1) return false;
		watchdogReset();
		delay(100);
	}
	CCSG_DEBUG_PRINTLN("[max17262] POR flag has been reset");

	delay(400); // per datasheet, ModelGauge m5 Algorithm Output Registers are valid 351 msec after the chip is configured.  See p.20.
	repCap = this->readRegisterRepeatedly(MAX17262_RepCap_Register, MAX17262_REPEAT_READS);
	if (repCap == -1) return false;
	CCSG_DEBUG_PRINTF("[max17262] RepCap register: 0x%04X\r\n", repCap);
	repSoC = this->readRegisterRepeatedly(MAX17262_RepSOC_Register, MAX17262_REPEAT_READS);
	if (repSoC == -1) return false;
	CCSG_DEBUG_PRINTF("[max17262] RepSoC register: 0x%04X\r\n", repSoC);
	tte = this->readRegisterRepeatedly(MAX17262_TTE_Register, MAX17262_REPEAT_READS);
	if (tte == -1) return false;
	CCSG_DEBUG_PRINTF("[max17262] TTE register:    0x%04X\r\n", tte);
	// CCSG_DEBUG_PRINTF("[max17262] Voltage: %2f\r\n", (float)this->avgVoltageMV() / 1000.0);

	delay(1000);

	// Valerie: Think it'll woik?
	// Miracle Max: It'd take a miracle
	statusPOR = this->readRegisterWithRetry(MAX17262_Status_Register, MAX17262_REPEAT_READS, 10);
	if (statusPOR == -1) return false;
	// CCSG_DEBUG_PRINTLN("[max17262] K");

	if ((statusPOR & 0x02) == 0)
	{
		CCSG_DEBUG_PRINTF("[max17262] Voltage: %2f\r\n", (float)this->avgVoltageMV() / 1000.0);
		CCSG_DEBUG_PRINTLN("[max17262] Power-on-reset completed successfully");
		return true;
	}
	else
	{
		CCSG_DEBUG_PRINTLN("[max17262] Power-on-reset did not complete successfully");
		return false; 
	}
}


void CCSG_MAX17262::reBeginI2C()
{
	i2cBus->begin();
}


// Result is in mV
uint16_t CCSG_MAX17262::avgVoltageMV(void)
{
	float mv;
	uint16_t cellVoltage = this->readRegisterRepeatedly(MAX17262_VCell_Register, MAX17262_REPEAT_READS);
	CCSG_DEBUG_PRINTF("[max17262] VCell reading: 0x%04X\r\n", cellVoltage);

	uint16_t avgVoltage = this->readRegisterRepeatedly(MAX17262_AvgVCell_Register, MAX17262_REPEAT_READS);
	CCSG_DEBUG_PRINTF("[max17262] AvgV  reading: 0x%04X\r\n", avgVoltage);

	mv = (float)avgVoltage * MAX17262_MV_PER_LSB;  // This conversion factor is a float
	return (uint16_t)mv;
}

// Result is in mV
uint16_t CCSG_MAX17262::minVoltageMV(void)
{
	uint16_t mv;

	mv = (this->readRegisterRepeatedly(MAX17262_MaxMinVolt_Register, MAX17262_REPEAT_READS) & 0xff) * MAX17262_MV_PER_LSB_MIN_MAX_REG;
	return mv;
}

// Result is in mV
uint16_t CCSG_MAX17262::maxVoltageMV(void)
{
	uint16_t mv;

	mv = ((this->readRegisterRepeatedly(MAX17262_MaxMinVolt_Register, MAX17262_REPEAT_READS) & 0xff00) >> 8) * MAX17262_MV_PER_LSB_MIN_MAX_REG;
	return mv;
}

// Remember: current is signed
int CCSG_MAX17262::avgCurrentUA(void)
{
	float current;

	current = (float)(this->readRegisterRepeatedly(MAX17262_AvgCurrent_Register, MAX17262_REPEAT_READS)) * MAX17262_UA_PER_LSB;  // This conversion factor is a float
	return (int)current;
}

uint32_t CCSG_MAX17262::uAhExpended(void)
{
	uint16_t uAhDelta = 0;

	// Maxim states the units of the QH register are uAH.  So the computation here should be right.
	// uint16_t qhRegisterNew = this->readRegisterRepeatedly(MAX17262_QH_Register, MAX17262_REPEAT_READS);  // 0xFFFF -> 0
	uint16_t qhRegisterNew = this->readRegisterRepeatedly(MAX17262_QH_Register, 1);  // 0xFFFF -> 0

	// The QH register will wrap around -- counting down from 0xFFFF to zero, then back to 0xFFFF.
	// Catch this case and deal with it properly

	// Example:
	//    qhRegisterOld was 0x0001
	//    qhRegisterNew is  0xFFFE
	//    uAhDelta is 0x0001 + (0xFFFF - 0xFFFE) = 0x0002
	if (qhRegisterNew > qhRegisterOld)
		uAhDelta = qhRegisterOld + (0xFFFF - qhRegisterNew);
	else
		uAhDelta = qhRegisterOld - qhRegisterNew;
	CCSG_DEBUG_PRINTF("[max17262] qhRegisterOld: %u\r\n", qhRegisterOld);
	CCSG_DEBUG_PRINTF("[max17262] qhRegisterNew: %u\r\n", qhRegisterNew);
	CCSG_DEBUG_PRINTF("[max17262] uAh delta:     %u\r\n", uAhDelta);
	qhRegisterOld = qhRegisterNew;

	softCapuAh -= uAhDelta; // update our capacity meter

	return (uint32_t)uAhDelta;
}

uint16_t CCSG_MAX17262::remainingCapacityPercent(void)
{
	// float capacityNow;
	// uint16_t repCap;

	// repCap = this->readRegisterRepeatedly(MAX17262_RepCap_Register, MAX17262_REPEAT_READS);
	// CCSG_DEBUG_PRINTF("[max17262] RepCap register: 0x%04X\r\n", repCap);
	// capacityNow = (float)repCap * MAX17262_MAH_PER_LSB;
	// return (uint16_t)(100.0 * capacityNow / capacitymAhFloat);

	return (uint16_t)(100 * softCapuAh / softCapuAhAtReset);
}

// reads the 16 bit value from the given register, -1 if failure
int CCSG_MAX17262::readRegister(uint8_t reg)
{
	// iaddress  	Internal register address
	// isize  		Number of internal address bytes (in this case, the register address is one byte)
	uint8_t bytesRetrieved = i2cBus->requestFrom((uint8_t)devAddr, (uint8_t) 2, (uint8_t)reg, (uint8_t) 1, (uint8_t) true);
	if (bytesRetrieved != 2)
	{
		//CCSG_DEBUG_PRINTF("[max17262] I2C was asked to retrieve 2 bytes but got %u\r\n", bytesRetrieved);
		return -1;
	}
    return (uint16_t)(i2cBus->read() | (i2cBus->read() << 8));  // The first byte read is interpreted as the LSB
}

// reads the 16 bit value from the given register, -1 if failure
int CCSG_MAX17262::readRegisterWithRetry(uint8_t reg, uint32_t retryAttempts, uint32_t delayMsec)
{
	bool hadFailure = false;
	int response = this->readRegister(reg);
	while (response == -1)
	{
		if (!hadFailure)
		{
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C readRegisterWithRetry() begins"));
			hadFailure = true;
		}

		if (retryAttempts == 0)
		{
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C readRegisterWithRetry() exhausted retry attempts"));
			return response;
		}
		else
		{
			retryAttempts--;
			delay(delayMsec);

			CCSG_DEBUG_PRINTLN(F("[max17262]     I2C readRegisterWithRetry() retrying"));
			response = this->readRegister(reg);
		}
	}
	if (hadFailure)
	{
		if (response != -1)
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C readRegisterWithRetry() succeeded"));
		else
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C readRegisterWithRetry() failed"));
	}
	return response;
}

// Read the named reg repeatedly until the same value is read of the retry
// counter has been exhausted.  Return the last value read.
int CCSG_MAX17262::readRegisterRepeatedly(uint8_t reg, uint32_t retryCounter)
{
	int oldValue = this->readRegisterWithRetry(reg, MAX17262_REPEAT_READS, 10);
	if (oldValue == -1) return -1;
	int newValue = this->readRegisterWithRetry(reg, MAX17262_REPEAT_READS, 10);
	if (newValue == -1) return -1;
	while(oldValue != newValue)
	{
		retryCounter--;
		if (retryCounter == 0)
			return newValue;
		else
		{
			oldValue = newValue;
			newValue = this->readRegisterWithRetry(reg, MAX17262_REPEAT_READS, 10);
			if (newValue == -1) return -1;
		}
	}
	return newValue;
}


// writes the 16 bit value to the given register
uint32_t CCSG_MAX17262::writeRegister(uint8_t reg, uint16_t val)
{
	i2cBus->beginTransmission(devAddr);
	i2cBus->write(reg);
	i2cBus->write(val & 0xff);  // low byte
	i2cBus->write(val >> 8);    // high byte
	uint8_t error = i2cBus->endTransmission(true);

	switch (error)
	{
		case 2:
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C endTransmission(): NAK on address transmission"));
			break;
		case 3:
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C endTransmission(): NAK on data transmission"));
			break;			
		case 4:
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C endTransmission(): error on finishing up"));
			break;	
		default:
			break;
	}
	return error;
}

uint32_t CCSG_MAX17262::writeRegisterWithRetry(uint8_t reg, uint16_t val, uint32_t retryAttempts, uint32_t delayMsec)
{
	bool hadFailure = false;
	uint8_t error = this->writeRegister(reg, val);

	while (error != 0)
	{
			if (!hadFailure)
		{
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C writeRegisterWithRetry() begins"));
			hadFailure = true;
		}
		if (retryAttempts == 0)
		{
			return error;
		}
		else
		{
			retryAttempts--;
			delay(delayMsec);
			CCSG_DEBUG_PRINTLN(F("[max17262]     I2C writeRegisterWithRetry() retrying"));
			error = this->writeRegister(reg, val);
		}
	}
	if (hadFailure)
	{
		if (error == 0)
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C writeRegisterWithRetry() succeeded"));	
		else
			CCSG_DEBUG_PRINTLN(F("[max17262]   I2C writeRegisterWithRetry() failed"));
	}
	return error;
}

void CCSG_MAX17262::writeAndVerifyRegister(uint8_t reg, uint16_t val)
{
	uint16_t readBackVal;

	this->writeRegister(reg, val);
	readBackVal = this->readRegister(reg);
	if (val != readBackVal)
		CCSG_DEBUG_PRINTF("[max17262] ERROR: readback value 0x%X does not match value written 0x%X\r\n", readBackVal,val);
}