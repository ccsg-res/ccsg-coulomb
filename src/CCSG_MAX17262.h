/*******************************************************************************
 * CCSG MAX17262 Driver
 *
 * Copyright (c) 2020 by Bob Iannucci
 *******************************************************************************/

#ifndef __CCSG_MAX17262_H__
#define __CCSG_MAX17262_H__

#include <Wire.h>
#include "CCSG_BoardConfig.h"

// Arduino Wire uses 7 bit addresses:  https://www.arduino.cc/en/reference/wire
#define CCSG_MAX17262_I2C_7B_ADDRESS	0x36

#define MAX17262_MV_PER_LSB				0.078125
#define MAX17262_MV_PER_LSB_MIN_MAX_REG	20

#define MAX17262_UA_PER_LSB				156.25
#define MAX17262_MA_PER_LSB_MIN_MAX		160		// not too useful...

#define MAX17262_MAH_PER_LSB			0.5

#define MAX17262_AIN_Register 			0x27
#define MAX17262_Age_Register 			0x07
#define MAX17262_AtAvCap_Register 		0xDF
#define MAX17262_AtAvSOC_Register 		0xDE
#define MAX17262_AtQResidual_Register 	0xDC
#define MAX17262_AtRate_Register 		0x04
#define MAX17262_AtTTE_Register 		0xDD
#define MAX17262_AvCap_Register 		0x1F
#define MAX17262_AvgCurrent_Register	0x0B
#define MAX17262_AvgVCell_Register		0x19
#define MAX17262_AvSOC_Register 		0x0E
#define MAX17262_CGTempCo_Register 		0xB8
#define MAX17262_CGain_Register 		0x2E
#define MAX17262_COff_Register 			0x2F
#define MAX17262_Command_Register 		0x60
#define MAX17262_Config2_Register 		0xBB
#define MAX17262_Config_Register 		0x1D
#define MAX17262_ConvgCfg_Register 		0x49
#define MAX17262_Curve_Register 		0xB9
#define MAX17262_Cycles_Register 		0x17
#define MAX17262_DesignCap_Register 	0x18
#define MAX17262_DevName_Register 		0x21
#define MAX17262_FStat_Register 		0x3D
#define MAX17262_FilterCfg_Register 	0x29
#define MAX17262_FullCapNom_Register 	0x23
#define MAX17262_FullCapRep_Register 	0x10
#define MAX17262_FullCap_Register 		0x35
#define MAX17262_FullSOCThr_Register 	0x13
#define MAX17262_Gain_Register 			0x2C
#define MAX17262_HibCfg_Register 		0xBA
#define MAX17262_IChgTerm_Register 		0x1E
#define MAX17262_LearnCfg_Register 		0x28
#define MAX17262_MPPCurrent_Register 	0xD9
#define MAX17262_PeakPower_Register 	0xD4
#define MAX17262_MaxMinCurr_Register	0x1C
#define MAX17262_MaxMinVolt_Register	0x1B
#define MAX17262_MinSysVoltage_Register 0xD8
#define MAX17262_MiscCfg_Register 		0x2B
#define MAX17262_MixCap_Register 		0x0F
#define MAX17262_MixSOC_Register 		0x0D
#define MAX17262_ModelCfg_Register 		0xDB
#define MAX17262_OCVTable0_Register 	0x80	// 0..15
#define MAX17262_PackResistance_Register 0xD6
#define MAX17262_QH_Register 			0x4D
#define MAX17262_QRTable00_Register 	0x12	// 0..31
#define MAX17262_QResidual_Register 	0x0C
#define MAX17262_RCell_Register 		0x14
#define MAX17262_RComp0_Register 		0x38
#define MAX17262_RGain_Register 		0x43
#define MAX17262_RelaxCfg_Register 		0x2A
#define MAX17262_RepCap_Register 		0x05
#define MAX17262_RepSOC_Register 		0x06
#define MAX17262_RippleCfg_Register 	0xBD
#define MAX17262_SPPCurrent_Register 	0xDA
#define MAX17262_ShdnTimer_Register 	0x3F
#define MAX17262_Status2_Register 		0xB0
#define MAX17262_Status_Register 		0x00
#define MAX17262_SusPeakPower_Register 	0xD5
#define MAX17262_SysResistance_Register 0xD7
#define MAX17262_TTE_Register 			0x11
#define MAX17262_TTF_Register 			0x20
#define MAX17262_TempCo_Register 		0x39
#define MAX17262_TimerH_Register 		0xBE
#define MAX17262_Timer_Register 		0x3E
#define MAX17262_Toff_Register 			0x2D
#define MAX17262_VCell_Register			0x19
#define MAX17262_VEmpty_Register 		0x3A
#define MAX17262_VFOCV_Register 		0xFB
#define MAX17262_VFRemCap_Register 		0x4A
#define MAX17262_VFSOC_Register 		0xFF
#define MAX17262_VRipple_Register 		0xBC
#define MAX17262_XTable0_Register 		0x90	// 0..15
#define MAX17262_dPAcc_Register 		0x46
#define MAX17262_dQAcc_Register 		0x05

typedef enum MAX17262_Lithium_Chemistry_t
{
	MAX17262_Lithium_Cobalt_Oxide = 0,
	MAX17262_Lithium_NCR_NCA = 2,
	MAX17262_Lithium_Iron_Phosphate = 6
} MAX17262_Lithium_Chemistry_t;

class CCSG_MAX17262
{
    public:

        CCSG_MAX17262(MAX17262_Lithium_Chemistry_t chem, uint16_t capmAh, float cv, float ev);

		bool init(TwoWire *i2c);

		// Values in mV
		uint16_t avgVoltageMV(void);
		uint16_t minVoltageMV(void);
		uint16_t maxVoltageMV(void);

		// Values in uA
		int avgCurrentUA(void);

		// Values in uAh
		uint32_t uAhExpended(void);

		// Values as integer percent
		uint16_t remainingCapacityPercent(void);

		// call after coming back from sleep
		void reBeginI2C();														

    private:

		TwoWire *i2cBus;
		uint8_t devAddr = CCSG_MAX17262_I2C_7B_ADDRESS;
		uint32_t softCapuAh;
		uint32_t softCapuAhAtReset;
		uint16_t capacitymAh;
		float capacitymAhFloat;
		float emptyVolts;
		float chargeVolts;
		float recoveryVolts;
		MAX17262_Lithium_Chemistry_t lithiumChemistry;
		uint16_t qhRegisterOld = 0xFFFF;

		int readRegister(uint8_t reg);											// reads the 16 bit value from the given register; -1 if error
		int readRegisterWithRetry(uint8_t reg, uint32_t retryAttempts, uint32_t delayMsec);
		int readRegisterRepeatedly(uint8_t reg, uint32_t retryCounter);  		// see comments above source

		uint32_t writeRegister(uint8_t reg, uint16_t val);						// writes the 16 bit value to the given register, nonzero if error
		uint32_t writeRegisterWithRetry(uint8_t reg, uint16_t val, uint32_t retryAttempts,  uint32_t delayMsec);
		void writeAndVerifyRegister(uint8_t reg, uint16_t val);					// writes with readback and check, -1 if fail

		bool power_on_reset();
};

#endif
